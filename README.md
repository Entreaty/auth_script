# README #

Simple parser for saving page for state "logged" on website 
http://etsgroup.ru/

### Installation ###

* Copy project files from repository to your project folder 
* Open this folder and execute command >> composer update

### Usage ###
```
$parser = new SimpleParser('your_login', 'your_password', 'name_of_file'); 
$parser->saveHTML();
```