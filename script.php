<?php
/**
 * Используем библиотеку для парсинга html странички по тегу и имени, как на jQuery
 */

require_once 'vendor/simple-html-dom/simple-html-dom/simple_html_dom.php';

class SimpleParser
{

	/**
	 * Объявляем переменные
	 *
	 * @var string
	 */
	private $url = "http://etsgroup.ru/";
	private $urlAuth = "http://etsgroup.ru/customer_sessions";
	private $login = null;
	private $password = null;
	private $token = null;
	private $FileName = null;

	/**
	 * SimpleParser конструктор.
	 *
	 * @param string $login
	 * @param string $password
	 * @param string $FileName
	 */
	function __construct($login, $password, $FileName)
	{
		$this->login = $login;
		$this->password = $password;
		$this->FileName = 'temp/'.$FileName;
	}

	/**
	 * Сохраняем в файл html-код главной страницы сайта в авторизованном состоянии.
	 */
	public function saveHTML()
	{
		try {
			$content = $this->getContent();
			$this->saveToFile($content);
		} catch (Exception $e) {
			echo $e;
		}
	}

	/**
	 * POST запрос с необходимыми параметрами для авторизации
	 *
	 * @return mixed
	 * @throws Exception
	 */
	private function getContent()
	{
		try {
			$this->getToken();
			if ($this->token != null) {
				$curl = curl_init();

				curl_setopt_array($curl, [
					CURLOPT_URL => $this->urlAuth,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_FOLLOWLOCATION => 1,    // Сайт использует редиректы, обязательно следовать за ними
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS => [
						'utf8' => '✓',
						'authenticity_token' => $this->token,
						'customer_session[login]' => $this->login,
						'customer_session[password]' => $this->password
					],
					CURLOPT_HTTPHEADER => [
						"cache-control: no-cache",
						"content-type: multipart/form-data; boundary=---011000010111000001101001",
						"postman-token: 01131b33-c85a-2bdd-76af-50697bc4a27a"
					],
					CURLOPT_COOKIEJAR => dirname(__FILE__) . '/temp/cookie.txt',
					CURLOPT_COOKIEFILE => dirname(__FILE__) . '/temp/cookie.txt'
				]);

				$response = curl_exec($curl);
				$err = curl_error($curl);

				curl_close($curl);

				if ($err) {
					throw new Exception("cURL Error #: " . $err);
				} else {
					return $response;
				}
			}
		} catch (Exception $e) {
			throw $e;
		}

	}

	/**
	 * Получаем токен
	 *
	 * @throws Exception
	 */
	private function getToken()
	{
		$html = file_get_html($this->url);

		if ($html) {
			// Используем подключенную библиотеку для поиска токена
			$this->token = $html->find('input[name=authenticity_token]')[0];
		} else {
			throw new Exception('Not found token at url: ' . $this->url);
		}
	}

	/**
	 * Записываем в файл html-код главной страницы сайта в авторизованном состоянии.
	 *
	 * @param $content
	 * @throws Exception
	 */
	private function saveToFile($content)
	{
		$fp = fopen($this->FileName, "w+");
		$test = fwrite($fp, $content);
		fclose($fp);
		if (!$test) throw new Exception('Unable to open/save file ' . $this->FileName);
	}
}

$a = new SimpleParser('info@qwep.ru', 'kVMu0pxoei', 'index.html');
$a->saveHTML();